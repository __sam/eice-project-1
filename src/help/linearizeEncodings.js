export default linearizeEncodings;

function linearizeEncodings(t){var e=[];return function t(n){if(Array.isArray(n))for(let e=0;e<n.length;e++)t(n[e]);else n&&(n.text=n.text||"",n.data=n.data||"",e.push(n))}(t),e}
